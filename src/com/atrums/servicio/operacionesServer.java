package com.atrums.servicio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.atrums.sri.ws.operaciones;

public class operacionesServer {

  final private String strKeyCert = "/opt/certificadosri/cacerts";
  final private operaciones ope = new operaciones();

  public Hashtable<String, String> documentoSRI(String strDoc, String strClie,
      String strClaveAccesso, String strFacId, String strDocId, String strDocStatus) {

    /*
     * Variable que almacena los objetos resultados
     */
    Hashtable<String, String> stbDatos = new Hashtable<String, String>();

    Date date = new Date();
    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    System.out.println("Tramitando documento " + strFacId + ", " + hourdateFormat.format(date));

    /*
     * Consulta a las operaciones de recebci�n y autorizaci�n de documento
     */

    if (strDocStatus.equals("PD") || strDocStatus.equals("NE")) {
      stbDatos = ope.procesarDocumentoXML(strDoc, strClie, strKeyCert, strClaveAccesso, strFacId,
          strDocId);
    } else if (strDocStatus.equals("RZ") || strDocStatus.equals("RG")) {
      stbDatos = ope.procesarDocumentoRechaXML(strDoc, strClie, strKeyCert, strClaveAccesso,
          strFacId, strDocId);
    } else if (strDocStatus.equals("RC") || strDocStatus.equals("AP")) {
      stbDatos = ope.procesarDocumentoAutXML(strDoc, strClie, strKeyCert, strClaveAccesso,
          strFacId, strDocId);
    }

    return new Hashtable<String, String>(stbDatos);
  }
}
