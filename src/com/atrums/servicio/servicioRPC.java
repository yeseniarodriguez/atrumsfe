package com.atrums.servicio;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.WebServer;

import com.atrums.sri.ws.operaciones;

/**
 * Servlet implementation class servicioRPC
 */
public class servicioRPC extends HttpServlet implements Servlet {
  private static final int port = 3530;
  final private String strKeyCert = "/opt/certificadosri/cacerts";
  private static final long serialVersionUID = 1L;
  operaciones ope = new operaciones();

  /**
   * @see HttpServlet#HttpServlet()
   */
  public servicioRPC() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
  }

  public void init(ServletConfig config) throws ServletException {
    System.out.println("Creando Servicio RCP");
    WebServer server = new WebServer(port);
    server.addHandler("operacionesServer", new operacionesServer());
    server.setParanoid(false);
    server.start();
    System.out.println("Servicio RPC iniciado");
    System.out.println("RPC aceptando solicitudes hasta que se pare el tomcat");

    ope.actualizarTransTotal();

    Timer tmrTiempo = new Timer();

    TimerTask ttk = new TimerTask() {

      @Override
      public void run() { // TODO Auto-generated method stub operaciones ope = new
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        System.out.println(hourdateFormat.format(date) + ": Ejecutando Operacion en Background");
        ope.procesarDocumentosXML(strKeyCert);
      }
    };

    tmrTiempo.schedule(ttk, 0, 100000);

  }
}
