package com.atrums.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.sql.SQLException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.DocumentException;

public class pruebaConexionDirecta {

  final static Integer intNumMax = 15;

  public static void main(String[] args) throws Exception {
    System.setProperty("javax.net.ssl.keyStore",
        "C:\\Program Files\\Java\\jre7\\lib\\security\\cacerts\\");
    System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
    System.setProperty("javax.net.ssl.trustStore",
        "C:\\Program Files\\Java\\jre7\\lib\\security\\cacerts");
    System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

    // System.out.print(UUID.randomUUID().toString().replace("-", "").toUpperCase());

    // recepcionXML(0);

    autorizacionXML();

  }

  private static void autorizacionXML() throws UnsupportedOperationException, SOAPException,
      TransformerException, InterruptedException, MalformedURLException, DocumentException {
    SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
    SOAPConnection soapCon = soapConF.createConnection();

    URL endPoint = new URL(null,
        "https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl",
        new URLStreamHandler() {

          @Override
          protected URLConnection openConnection(URL u) throws IOException {
            // TODO Auto-generated method stub
            URL clone_url = new URL(u.toString());
            HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url
                .openConnection();

            clone_urlconConnection.setConnectTimeout(10000);
            clone_urlconConnection.setReadTimeout(10000);

            return clone_urlconConnection;
          }
        });

    SOAPMessage soapMes;

    try {
      soapMes = soapCon.call(
          createSoapReqAutXML("1601201507179133199000120020010000206980000123411"), endPoint);

      String strEstado = ""; /*
                              * "CLAVE DE ACCESO EN PROCESAMIENTO  - La clave de acceso 1601201501179133199000120020010000509130000123410  esta en procesamiento VALOR DEVUELTO POR EL PROCEDIMIENTO: SI"
                              * ;
                              * 
                              * if (strEstado.indexOf("CLAVE DE ACCESO EN PROCESAMIENTO") != -1) {
                              * System.out.println("Encontrado procesamiento"); }
                              */

      int intNumeroItems = soapMes.getSOAPBody().getElementsByTagName("estado").getLength();

      int intAuxi = intNumeroItems - 1;
      int intposAut = 0;

      while (intAuxi > -1) {
        strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(intAuxi)
            .getFirstChild().getNodeValue();

        if (strEstado.equals("AUTORIZADO")) {
          intposAut = intAuxi;
        }
        intAuxi--;
      }

      String strMensaje = "";

      strMensaje = soapMes.getSOAPBody().getElementsByTagName("estado").item(intposAut)
          .getFirstChild().getNodeValue();

      System.out.println("" + strMensaje);

      strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(intposAut)
          .getFirstChild().getNodeValue();

      System.out.println(strEstado);

      String strDatos = soapMes.getSOAPBody().getElementsByTagName("fechaAutorizacion")
          .item(intposAut).getFirstChild().getNodeValue();

      System.out.println(strDatos);

      if (strDatos.indexOf("PROCESAMIENTO") != -1) {
        System.out.println("RC");
      } else {
        // System.out.println("RZ");
      }

      // System.out.println(strDatos);

      String strDatos2 = "";

      try {
        strDatos2 = soapMes.getSOAPBody().getElementsByTagName("informacionAdicional").item(0)
            .getFirstChild().getNodeValue();
      } catch (Exception e) {
        // TODO: handle exception
      }

      System.out.println(strDatos2);

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static void recepcionXML(Integer intNumIntentos) throws UnsupportedOperationException,
      SOAPException, SQLException, TransformerException, MalformedURLException {
    SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
    SOAPConnection soapCon = soapConF.createConnection();

    URL endPoint = new URL(null,
        "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl",
        new URLStreamHandler() {

          @Override
          protected URLConnection openConnection(URL u) throws IOException {
            // TODO Auto-generated method stub
            URL clone_url = new URL(u.toString());
            HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url
                .openConnection();

            clone_urlconConnection.setConnectTimeout(10000);
            clone_urlconConnection.setReadTimeout(10000);

            return clone_urlconConnection;
          }
        });

    try {
      SOAPMessage soapMes = soapCon.call(createSoapReqRecXML(documentoXmlConSQL()), endPoint);

      printSoapRes(soapMes);
    } catch (Exception e) {
      // TODO: handle exception
      if (intNumIntentos <= intNumMax) {
        recepcionXML(intNumIntentos + 1);
      } else {
        System.out.println("El servidor no esta disponible");
      }
    }
  }

  private static SOAPMessage createSoapReqRecXML(String strXml) throws SOAPException, IOException {
    MessageFactory messageFac = MessageFactory.newInstance();
    SOAPMessage soapMes = messageFac.createMessage();
    SOAPPart soapPart = soapMes.getSOAPPart();

    SOAPEnvelope soapEnv = soapPart.getEnvelope();
    soapEnv.addNamespaceDeclaration("ec", "http://ec.gob.sri.ws.recepcion");

    SOAPBody soapBod = soapEnv.getBody();

    SOAPElement soapEle = soapBod.addChildElement("validarComprobante", "ec");
    SOAPElement soapEle1 = soapEle.addChildElement("xml");
    soapEle1.addTextNode(strXml);

    soapMes.saveChanges();

    System.out.println("Request Soap Message = ");
    soapMes.writeTo(System.out);

    return soapMes;
  }

  private static SOAPMessage createSoapReqAutXML(String strClave) throws SOAPException, IOException {
    MessageFactory messageFac = MessageFactory.newInstance();
    SOAPMessage soapMes = messageFac.createMessage();
    SOAPPart soapPart = soapMes.getSOAPPart();

    SOAPEnvelope soapEnv = soapPart.getEnvelope();
    soapEnv.addNamespaceDeclaration("ec", "http://ec.gob.sri.ws.autorizacion");

    SOAPBody soapBod = soapEnv.getBody();

    SOAPElement soapEle = soapBod.addChildElement("autorizacionComprobante", "ec");
    SOAPElement soapEle1 = soapEle.addChildElement("claveAccesoComprobante");
    soapEle1.addTextNode(strClave);

    soapMes.saveChanges();

    System.out.println("Request Soap Message = ");
    // soapMes.writeTo(System.out);

    return soapMes;
  }

  private static void printSoapRes(SOAPMessage soapMes) throws SOAPException, TransformerException {
    TransformerFactory transformerFac = TransformerFactory.newInstance();
    Transformer transfor = transformerFac.newTransformer();
    Source sourceCon = soapMes.getSOAPPart().getContent();
    System.out.println("\nResponse SOAP Message = ");
    StreamResult result = new StreamResult(System.out);
    transfor.transform(sourceCon, result);
  }

  private static String documentoXmlConSQL() throws SQLException, IOException {
    String strDocumento = "";

    /*
     * String url = "jdbc:postgresql://186.5.101.204:5432/openbravo"; Connection con =
     * DriverManager.getConnection(url, "postgres", "mdt2014+"); Statement s =
     * con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
     * 
     * String strSql = "select encode(em_atecfe_documento_xml,'base64') " +
     * "as dato1 from c_invoice " + "where c_invoice_id = '5BD28B616AA245F2A75DF57F07909C27'";
     * 
     * ResultSet rs = s.executeQuery(strSql);
     * 
     * if (rs != null) { rs.next(); strDocumento = rs.getString("dato1"); }
     * 
     * rs.close(); s.close(); con.close();
     */

    File fl = new File("C:\\firma\\2509201401179133199000110020010000472410000123415.xml");

    byte[] bytes = new byte[(int) fl.length()];

    FileInputStream is = new FileInputStream(fl);

    is.read(bytes);
    is.close();

    byte[] bytesen = Base64.encodeBase64(bytes);

    String encodedString = new String(bytesen, "UTF-8");

    return encodedString;
  }
}
