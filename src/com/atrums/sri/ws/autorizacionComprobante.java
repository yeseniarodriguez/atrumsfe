package com.atrums.sri.ws;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Hashtable;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;

public class autorizacionComprobante {

  /*
   * Funci�n para realizar el consumo del Web service de autorizaci�n
   */
  public static String autorizacionXML(String strwsdlRec, String strNameSpace, String strClave,
      Integer intNumInt, Integer intMaxNumInt, List<String> lsList) {

    try {
      /*
       * Realizando la conexion con el Web service
       */
      SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();

      /*
       * Realizando el thread para verificar la conexion
       */
      System.out.println("Recibiendo del Ambiente: " + strwsdlRec);
      
      URL endPoint = new URL(null, strwsdlRec, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          // TODO Auto-generated method stub
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          /*
           * Asignando tiempos de respuesta maximos
           */
          clone_urlconConnection.setConnectTimeout(60000);
          clone_urlconConnection.setReadTimeout(60000);
          return clone_urlconConnection;
        }
      });

      /*
       * Retrazo de tiempo de ejecuci�n para que el web service de una respuesta de autorizaci�n
       */
      Thread.sleep(1500);

      /*
       * Realizando la consulta al Web service de autorizaci�n
       */
      SOAPMessage soapMes = soapCon.call(createSoapReqAutXML(strNameSpace, strClave), endPoint);

      /*
       * Capturando que estado devolvio del Web service de la petici�n
       */
      String strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(0)
          .getFirstChild().getNodeValue();

      /*
       * Verificando que resultado dio el Web Service
       */
      if (strEstado.equals("NO AUTORIZADO")) {
        lsList.clear();

        /*
         * Armando el mensaje de respuesta
         */
        lsList.add(soapMes.getSOAPBody().getElementsByTagName("mensaje").item(1).getFirstChild()
            .getNodeValue());

        if (soapMes.getSOAPBody().getElementsByTagName("informacionAdicional") != null) {
          lsList.add(soapMes.getSOAPBody().getElementsByTagName("informacionAdicional").item(0)
              .getFirstChild().getNodeValue());
        }

        lsList.add(stringtostring64(soapMes.getSOAPBody().getElementsByTagName("comprobante")
            .item(0).getFirstChild().getNodeValue()));
        soapCon.close();
        return "2";
      } else if (strEstado.equals("AUTORIZADO")) {
        lsList.clear();

        /*
         * Armando el mensaje de respuesta
         */
        lsList.add(soapMes.getSOAPBody().getElementsByTagName("mensaje").item(1).getFirstChild()
            .getNodeValue());

        if (soapMes.getSOAPBody().getElementsByTagName("informacionAdicional") != null) {
          lsList.add(soapMes.getSOAPBody().getElementsByTagName("informacionAdicional").item(0)
              .getFirstChild().getNodeValue());
        }

        lsList.add(stringtostring64(soapMes.getSOAPBody().getElementsByTagName("comprobante")
            .item(0).getFirstChild().getNodeValue()));
        soapCon.close();
        return "1";
      }
    } catch (Exception e) {
      // TODO: handle exception

      /*
       * Verificando que cuantos intentos tiene realizado la consulta
       */
      if (intNumInt <= intMaxNumInt) {
        return (autorizacionXML(strwsdlRec, strNameSpace, strClave, intNumInt + 1, intMaxNumInt,
            lsList));
      } else {
        /*
         * Armando el mensaje de respuesta
         */
        lsList.clear();
        lsList.add("El Web Service no disponible del SRI para Autorizar el documento");
        return "0";
      }
    }
    return "0";
  }

  /*
   * Funci�n para realizar el consumo del Web service de autorizaci�n
   */
  public static boolean autorizacionDocXML(String strwsdlRec, String strNameSpace, String strClave,
      Integer intNumInt, Integer intMaxNumInt, Hashtable<String, String> hstDatos) {

    try {
      /*
       * Realizando la conexi�n con el Web service
       */
      SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();

      /*
       * Realizando el thread para verificar la conexion
       */
      System.out.println("Recibiendo del Ambiente: " + strwsdlRec);
      
      URL endPoint = new URL(null, strwsdlRec, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          // TODO Auto-generated method stub
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          /*
           * Asignando tiempos de respuesta maximos
           */
          clone_urlconConnection.setConnectTimeout(60000);
          clone_urlconConnection.setReadTimeout(60000);
          return clone_urlconConnection;
        }
      });

      /*
       * Retrazo de tiempo de ejecucion para que el web service de una respuesta de autorizaci�n
       */
      Thread.sleep(2500);

      /*
       * Realizando la consulta al Web service de autorizaci�n
       */
      SOAPMessage soapMes = soapCon.call(createSoapReqAutXML(strNameSpace, strClave), endPoint);

      /*
       * Capturando que estado devolvio del Web service de la petici�n
       */

      Integer intNumeroComprobantes = Integer.valueOf(soapMes.getSOAPBody()
          .getElementsByTagName("numeroComprobantes").item(0).getFirstChild().getNodeValue());

      if (intNumeroComprobantes > 0) {

        String strEstado = "";
        int intNumeroItems = soapMes.getSOAPBody().getElementsByTagName("estado").getLength();

        int intAuxi = intNumeroItems - 1;
        int intposAut = 0;

        while (intAuxi > -1) {
          strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(intAuxi)
              .getFirstChild().getNodeValue();

          if (strEstado.equals("AUTORIZADO")) {
            intposAut = intAuxi;
          }
          intAuxi--;
        }

        strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(intposAut)
            .getFirstChild().getNodeValue();

        /*
         * Verificando que resultado dio el Web Service
         */
        if (strEstado.equals("NO AUTORIZADO")) {

          /*
           * Armando el mensaje de respuesta
           */
          hstDatos.put(
              "doc",
              stringtostring64(soapMes.getSOAPBody().getElementsByTagName("comprobante")
                  .item(intposAut).getFirstChild().getNodeValue()));
          hstDatos.remove("mens");

          String Mensaje = "";

          try {
            Mensaje = Mensaje
                + soapMes.getSOAPBody().getElementsByTagName("mensaje").item(intposAut)
                    .getFirstChild().getNodeValue() + " - ";
          } catch (Exception e) {
            // TODO: handle exception
          }

          try {
            Mensaje = Mensaje
                + soapMes.getSOAPBody().getElementsByTagName("informacionAdicional")
                    .item(intposAut).getFirstChild().getNodeValue();
          } catch (Exception e) {
            // TODO: handle exception
          }

          if (Mensaje.length() > 254) {
            hstDatos.put("mens", Mensaje.substring(0, 254));
          } else {
            hstDatos.put("mens", Mensaje);
          }

          hstDatos.remove("dstd");

          if (Mensaje.indexOf("PROCESAMIENTO") != -1) {
            hstDatos.put("dstd", "RC");
          } else {
            hstDatos.put("dstd", "RZ");
          }

          soapCon.close();
          return true;
        } else if (strEstado.equals("AUTORIZADO")) {

          /*
           * Armando el mensaje de respuesta
           */

          String strMensajeAux = "";

          System.out.println("Numero de registro en soap: " + intposAut);

          hstDatos.put(
              "doc",
              stringtostring64Au(
                  soapMes.getSOAPBody().getElementsByTagName("comprobante").item(intposAut)
                      .getFirstChild().getNodeValue(),
                  soapMes.getSOAPBody().getElementsByTagName("estado").item(intposAut)
                      .getFirstChild().getNodeValue(),
                  soapMes.getSOAPBody().getElementsByTagName("numeroAutorizacion").item(0)
                      .getFirstChild().getNodeValue(),
                  soapMes.getSOAPBody().getElementsByTagName("fechaAutorizacion").item(intposAut)
                      .getFirstChild().getNodeValue(),
                  soapMes.getSOAPBody().getElementsByTagName("ambiente").item(intposAut)
                      .getFirstChild().getNodeValue()));
          hstDatos.remove("mens");
          hstDatos.put("mens", "AUTORIZADO" + strMensajeAux);
          hstDatos.remove("std");
          hstDatos.put("std", "PD");
          hstDatos.remove("dstd");
          hstDatos.put("dstd", "AP");
          hstDatos.remove("numaut");
          hstDatos.put("numaut", soapMes.getSOAPBody().getElementsByTagName("numeroAutorizacion")
              .item(0).getFirstChild().getNodeValue());
          hstDatos.remove("fecaut");
          hstDatos.put("fecaut", soapMes.getSOAPBody().getElementsByTagName("fechaAutorizacion")
              .item(intposAut).getFirstChild().getNodeValue());
          soapCon.close();
          return true;
        } else {
          hstDatos.remove("mens");
          hstDatos.put("mens", "Ocurrio un error en el servicio del SRI");
          hstDatos.remove("dstd");
          hstDatos.put("dstd", "RC");
          return false;
        }
      } else {
        if (intNumInt <= intMaxNumInt) {
          return (autorizacionDocXML(strwsdlRec, strNameSpace, strClave, intNumInt + 1,
              intMaxNumInt, hstDatos));
        } else {
          /*
           * Armando el mensaje de respuesta
           */
          hstDatos.remove("mens");
          hstDatos
              .put(
                  "mens",
                  "El Servicio del SRI aun no tramita su documento, su documento sera procesado mas tarde automaticamente");
          hstDatos.remove("dstd");
          hstDatos.put("dstd", "RC");
          return false;
        }
      }
    } catch (Exception e) {
      // TODO: handle exception

      /*
       * Verificando que cuantos intentos tiene realizado la consulta
       */
      if (intNumInt <= intMaxNumInt) {
        return (autorizacionDocXML(strwsdlRec, strNameSpace, strClave, intNumInt + 1, intMaxNumInt,
            hstDatos));
      } else {
        /*
         * Armando el mensaje de respuesta
         */
        hstDatos.remove("mens");
        hstDatos
            .put("mens",
                "El Servicio del SRI no esta disponible, su documento sera procesado mas tarde automaticamente");
        hstDatos.remove("std");
        hstDatos.put("std", "PR");
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "RC");
        hstDatos.remove("numaut");
        hstDatos.put("numaut", "");
        hstDatos.remove("fecaut");
        hstDatos.put("fecaut", "");
        return false;
      }
    }
  }

  /*
   * Funci�n para la creaci�n de la consulta soap para el Web Service de autorizaci�n del documento
   * por el SRI
   */
  private static SOAPMessage createSoapReqAutXML(String strNameSpace, String strClave)
      throws SOAPException, IOException {
    MessageFactory messageFac = MessageFactory.newInstance();
    SOAPMessage soapMes = messageFac.createMessage();
    SOAPPart soapPart = soapMes.getSOAPPart();

    /*
     * Configuraci�n el Namespace del Web Service
     */
    SOAPEnvelope soapEnv = soapPart.getEnvelope();
    soapEnv.addNamespaceDeclaration("ec", strNameSpace);

    SOAPBody soapBod = soapEnv.getBody();

    /*
     * Configuraci�n de los parametros pasados al Web Service
     */
    SOAPElement soapEle = soapBod.addChildElement("autorizacionComprobante", "ec");
    SOAPElement soapEle1 = soapEle.addChildElement("claveAccesoComprobante");
    soapEle1.addTextNode(strClave);

    /*
     * Guardando los cambios de la configuracion
     */
    soapMes.saveChanges();

    return soapMes;
  }

  private static String stringtostring64(String strDocXML) throws IOException {
    File flDoc = File.createTempFile("documento", null);
    flDoc.deleteOnExit();

    final OutputFormat ofFormat = new OutputFormat();
    ofFormat.setEncoding("utf-8");

    Writer wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(flDoc), "UTF-8"));
    wr.write(strDocXML);
    wr.flush();
    wr.close();

    byte[] bytes = new byte[(int) flDoc.length()];

    FileInputStream is = new FileInputStream(flDoc);

    is.read(bytes);
    is.close();

    byte[] bytesen = Base64.encodeBase64(bytes);
    String strDat = new String(bytesen, "UTF-8");
    flDoc.delete();
    return strDat;
  }

  private static String stringtostring64Au(String strComprobante, String strEstado,
      String strNumeroAutorizacion, String strFechaAutorizacion, String strAmbiente)
      throws DocumentException, IOException {

    File flDoc = File.createTempFile("documento", ".xml", null);

    Document docXML = DocumentHelper.createDocument();

    final org.dom4j.io.OutputFormat ofFormat = org.dom4j.io.OutputFormat.createPrettyPrint();
    ofFormat.setEncoding("UTF-8");

    final Element elmact = docXML.addElement("autorizacion");

    elmact.addElement("estado").addText(strEstado);
    elmact.addElement("numeroAutorizacion").addText(strNumeroAutorizacion);
    elmact.addElement("fechaAutorizacion").addText(strFechaAutorizacion);
    elmact.addElement("ambiente").addText(strAmbiente);

    SAXReader reader = new SAXReader();
    Document docAux = reader.read(new StringReader(strComprobante));

    final Element elmcom = elmact.addElement("comprobante");
    elmcom.add(docAux.getRootElement());

    final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flDoc),
        "UTF-8"), ofFormat);
    writer.write(docXML);
    writer.flush();
    writer.close();

    byte[] bytes = new byte[(int) flDoc.length()];

    FileInputStream is = new FileInputStream(flDoc);

    is.read(bytes);
    is.close();

    byte[] bytesen = Base64.encodeBase64(bytes);
    String strDat = new String(bytesen, "UTF-8");
    flDoc.delete();
    return strDat;
  }
}