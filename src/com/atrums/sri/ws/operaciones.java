package com.atrums.sri.ws;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.atrums.entidades.clientes;
import com.atrums.entidades.temEnt;

public class operaciones {

  private String strWDSLRe = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
  private String strWDSLAu = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
  private String strNameSpaceRe = "http://ec.gob.sri.ws.recepcion";
  private String strNameSpaceAu = "http://ec.gob.sri.ws.autorizacion";

  private Calendar calendario = Calendar.getInstance();

  public void procesarDocumentosXML(String strLlaveSRIJava) {
    try {
      /*
       * Configurando el ambiente con el cerificado d�gital que se instalo en la aplicaci�n
       */

      /**
       * strLlaveSRIJava es el ambiente donde se encuentra el certificado digital del SRI instalado
       * en el java
       */

      System.setProperty("javax.net.ssl.keyStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
      System.setProperty("javax.net.ssl.trustStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

      Context initConte;

      /*
       * Cargando los datos del context
       */
      initConte = new InitialContext();
      Context envConte = (Context) initConte.lookup("java:/comp/env");

      DataSource dataSour = null;

      dataSour = (DataSource) envConte.lookup("jdbc/postgres");

      List<clientes> dtsClis = datosClis(dataSour);

      if (dtsClis.size() > 0) {
        for (int i = 0; i < dtsClis.size(); i++) {
          List<temEnt> dtsTemp = buscarDocumentos(dtsClis.get(i), dataSour);
          if (dtsTemp.size() > 0) {
            Date date = new Date();
            DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
            System.out.println("Tramitando " + dtsTemp.size() + " documentos, "
                + hourdateFormat.format(date));
            for (int j = 0; j < dtsTemp.size(); j++) {
              Hashtable<String, String> stbDatos = new Hashtable<String, String>();
              if (dtsTemp.get(j).getEstado().equals("RZ")
                  || dtsTemp.get(j).getEstado().equals("PD")
                  || dtsTemp.get(j).getEstado().equals("RG")) {
                stbDatos = procesarDocumentoRechaXML(dtsTemp.get(j).getDoc_xml(), dtsTemp.get(j)
                    .getCli_id(), strLlaveSRIJava, dtsTemp.get(j).getCla_acc(), dtsTemp.get(j)
                    .getDoc_id(), dtsTemp.get(j).getTipo_doc());
                strGuardarDocMen(stbDatos, dtsTemp.get(j).getDoc_id(), dtsClis.get(i), dataSour);
              } else if (dtsTemp.get(j).getEstado().equals("RC")) {
                stbDatos = procesarDocumentoAutXML(dtsTemp.get(j).getDoc_xml(), dtsTemp.get(j)
                    .getCli_id(), strLlaveSRIJava, dtsTemp.get(j).getCla_acc(), dtsTemp.get(j)
                    .getDoc_id(), dtsTemp.get(j).getTipo_doc());
                strGuardarDocMen(stbDatos, dtsTemp.get(j).getDoc_id(), dtsClis.get(i), dataSour);
              }
            }
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /*
   * funci�n para procesar documento XML desde el Servicio RPC
   */

  public Hashtable<String, String> procesarDocumentoXML(String strdocXML, String strClien,
      String strLlaveSRIJava, String strClaveAcceso, String strFac, String strDoc) {
    /*
     * Variable para guardar los datos que se generen en la base de datos de abajo
     */
    final Hashtable<String, String> hstDatos = new Hashtable<String, String>();

    try {
      /*
       * Configurando el ambiente con el cerificado d�gital que se instalo en la aplicaci�n
       */

      /**
       * strLlaveSRIJava es el ambiente donde se encuentra el certificado digital del SRI instalado
       * en el java
       */

      System.setProperty("javax.net.ssl.keyStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
      System.setProperty("javax.net.ssl.trustStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

      Context initConte;

      /*
       * Cargando los datos del context
       */
      initConte = new InitialContext();
      Context envConte = (Context) initConte.lookup("java:/comp/env");

      DataSource dataSour = null;

      dataSour = (DataSource) envConte.lookup("jdbc/postgres");

      System.out.println("Verificando ambiente");

      byte[] btydocenc;
      byte[] btydocdec;
      String strDocXMLdec = null;
      Document docAux = null;
      Element elemAux = null;

      btydocenc = strdocXML.getBytes();

      btydocdec = Base64.decodeBase64(btydocenc);

      if (btydocdec != null) {
        strDocXMLdec = new String(btydocdec);

        SAXReader reader = new SAXReader();

        try {
          docAux = reader.read(new StringReader(strDocXMLdec));
          elemAux = docAux.getRootElement();

          if (elemAux != null) {
        	System.out.println("Ambiente Documento: " + elemAux.element("infoTributaria").element("ambiente").getText());
        	  
            if (elemAux.element("infoTributaria").element("ambiente").getText().equals("2")) {
              this.strWDSLRe = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
              this.strWDSLAu = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
            }
          }

        } catch (DocumentException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      if (verificarCliente(strClien, hstDatos, dataSour)) {
        if (insertarTransa(strClien, strDoc, strFac, hstDatos, dataSour)) {

          if (hstDatos.get("rec").equals("N")) {
            if (recepcionComprobante.recepcionDocXML(strWDSLRe, strNameSpaceRe, strdocXML, 0, 2,
                hstDatos)) {
              actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                  hstDatos.get("mens"), dataSour, true);
              if (autorizacionComprobante.autorizacionDocXML(strWDSLAu, strNameSpaceAu,
                  strClaveAcceso, 0, 2, hstDatos)) {
                actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                    hstDatos.get("mens"), dataSour, false);
              } else {
                hstDatos.put("doc", strdocXML);
                actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                    hstDatos.get("mens"), dataSour, false);
              }
            } else {
              actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                  hstDatos.get("mens"), dataSour, false);
            }
          } else if (hstDatos.get("rec").equals("Y")) {
            if (autorizacionComprobante.autorizacionDocXML(strWDSLAu, strNameSpaceAu,
                strClaveAcceso, 0, 2, hstDatos)) {
              actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                  hstDatos.get("mens"), dataSour, false);
            } else {
              hstDatos.put("doc", strdocXML);
              actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                  hstDatos.get("mens"), dataSour, false);
            }
          }
        } else {
          hstDatos.put("doc", strdocXML);
        }
      } else {
        hstDatos.put("doc", strdocXML);
      }
    } catch (NamingException e) {
      // TODO Auto-generated catch block
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      hstDatos.put("numaut", "");
      hstDatos.put("doc", strdocXML);
      e.printStackTrace();
    }
    return hstDatos;
  }

  public Hashtable<String, String> procesarDocumentoRechaXML(String strdocXML, String strClien,
      String strLlaveSRIJava, String strClaveAcceso, String strFac, String strDoc) {
    /*
     * Variable para guardar los datos que se generen en la base de datos de abajo
     */
    final Hashtable<String, String> hstDatos = new Hashtable<String, String>();

    try {
      /*
       * Configurando el ambiente con el cerificado d�gital que se instalo en la aplicaci�n
       */

      /**
       * strLlaveSRIJava es el ambiente donde se encuentra el certificado digital del SRI instalado
       * en el java
       */

      System.setProperty("javax.net.ssl.keyStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
      System.setProperty("javax.net.ssl.trustStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

      Context initConte;

      /*
       * Cargando los datos del context
       */
      initConte = new InitialContext();
      Context envConte = (Context) initConte.lookup("java:/comp/env");

      DataSource dataSour = null;

      dataSour = (DataSource) envConte.lookup("jdbc/postgres");

      System.out.println("Verificando ambiente");

      byte[] btydocenc;
      byte[] btydocdec;
      String strDocXMLdec = null;
      Document docAux = null;
      Element elemAux = null;

      btydocenc = strdocXML.getBytes();

      btydocdec = Base64.decodeBase64(btydocenc);

      if (btydocdec != null) {
        strDocXMLdec = new String(btydocdec);

        SAXReader reader = new SAXReader();

        try {
          docAux = reader.read(new StringReader(strDocXMLdec));
          elemAux = docAux.getRootElement();

          if (elemAux != null) {
        	  System.out.println("Ambiente Documento: " + elemAux.element("infoTributaria").element("ambiente").getText());
        	
            if (elemAux.element("infoTributaria").element("ambiente").getText().equals("2")) {
              strWDSLRe = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
              strWDSLAu = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
            }
          }

        } catch (DocumentException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      if (verificarCliente(strClien, hstDatos, dataSour)) {
        if (buscarTransa(strClien, strDoc, strFac, hstDatos, dataSour)) {
          if (recepcionComprobante.recepcionDocXML(strWDSLRe, strNameSpaceRe, strdocXML, 0, 2,
              hstDatos)) {
            actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                hstDatos.get("mens"), dataSour, true);
            if (autorizacionComprobante.autorizacionDocXML(strWDSLAu, strNameSpaceAu,
                strClaveAcceso, 0, 2, hstDatos)) {
              actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                  hstDatos.get("mens"), dataSour, false);
            } else {
              hstDatos.put("doc", strdocXML);
            }
          } else {
            actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                hstDatos.get("mens"), dataSour, false);
          }
        } else {
          hstDatos.put("doc", strdocXML);
        }
      } else {
        hstDatos.put("doc", strdocXML);
      }
    } catch (NamingException e) {
      // TODO Auto-generated catch block
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      hstDatos.put("doc", strdocXML);
      e.printStackTrace();
    }
    return hstDatos;
  }

  public Hashtable<String, String> procesarDocumentoAutXML(String strdocXML, String strClien,
      String strLlaveSRIJava, String strClaveAcceso, String strFac, String strDoc) {
    /*
     * Variable para guardar los datos que se generen en la base de datos de abajo
     */
    final Hashtable<String, String> hstDatos = new Hashtable<String, String>();

    try {
      /*
       * Configurando el ambiente con el cerificado d�gital que se instalo en la aplicaci�n
       */

      /**
       * strLlaveSRIJava es el ambiente donde se encuentra el certificado digital del SRI instalado
       * en el java
       */

      System.setProperty("javax.net.ssl.keyStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
      System.setProperty("javax.net.ssl.trustStore", strLlaveSRIJava);
      System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

      Context initConte;

      /*
       * Cargando los datos del context
       */
      initConte = new InitialContext();
      Context envConte = (Context) initConte.lookup("java:/comp/env");

      DataSource dataSour = null;

      dataSour = (DataSource) envConte.lookup("jdbc/postgres");

      System.out.println("Verificando ambiente");

      byte[] btydocenc;
      byte[] btydocdec;
      String strDocXMLdec = null;
      Document docAux = null;
      Element elemAux = null;

      btydocenc = strdocXML.getBytes();

      btydocdec = Base64.decodeBase64(btydocenc);

      if (btydocdec != null) {
        strDocXMLdec = new String(btydocdec);

        SAXReader reader = new SAXReader();

        try {
          docAux = reader.read(new StringReader(strDocXMLdec));
          elemAux = docAux.getRootElement();

          if (elemAux != null) {
        	System.out.println("Ambiente Documento: " + elemAux.element("infoTributaria").element("ambiente").getText());
        	  
            if (elemAux.element("infoTributaria").element("ambiente").getText().equals("2")) {
              strWDSLRe = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantes?wsdl";
              strWDSLAu = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantes?wsdl";
            }
          }

        } catch (DocumentException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      if (verificarCliente(strClien, hstDatos, dataSour)) {
        if (buscarTransa(strClien, strDoc, strFac, hstDatos, dataSour)) {
          if (autorizacionComprobante.autorizacionDocXML(strWDSLAu, strNameSpaceAu, strClaveAcceso,
              0, 2, hstDatos)) {
            actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                hstDatos.get("mens"), dataSour, false);
          } else {
            actualizarTransEstado(hstDatos.get("clvtra"), hstDatos.get("dstd"),
                hstDatos.get("mens"), dataSour, false);
            hstDatos.put("doc", strdocXML);
          }
        } else {
          hstDatos.put("doc", strdocXML);
        }
      } else {
        hstDatos.put("doc", strdocXML);
      }
    } catch (NamingException e) {
      // TODO Auto-generated catch block
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      hstDatos.put("doc", strdocXML);
      e.printStackTrace();
    }
    return hstDatos;
  }

  /*
   * Funci�n que guarda los datos en el cliente devueltos del WS
   */
  private boolean strGuardarDocMen(Hashtable<String, String> hstDatos, String strDocID,
      clientes clsCli, DataSource dataSour) {
    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String mensaje2 = hstDatos.get("mens").replaceAll("'", "''");

      String strSql = "SELECT dblink_exec('dbname=" + clsCli.getBdd() + " hostaddr="
          + clsCli.getIp() + " user=" + clsCli.getUsr() + " password=" + clsCli.getPass()
          + " port=" + clsCli.getPuerto() + "',"
          + "'update c_invoice set em_atecfe_documento_xml = decode(''" + hstDatos.get("doc")
          + "'',''base64''), " + "em_atecfe_menobserror_sri = ''" + mensaje2 + "'', "
          + "em_atecfe_docaction = ''" + hstDatos.get("std") + "'', em_atecfe_docstatus = ''"
          + hstDatos.get("dstd") + "'', em_co_nro_aut_sri = ''" + hstDatos.get("numaut") + "'', "
          + "em_atecfe_fecha_autori = ''" + hstDatos.get("fecaut") + "'', "
          + "em_co_vencimiento_aut_sri = ";

      if (hstDatos.get("fecaut").equals("")) {
        strSql = strSql + "date(null)";
      } else {
        strSql = strSql + "date(''" + hstDatos.get("fecaut") + "'')";
      }

      strSql = strSql + " where c_invoice_id = ''" + strDocID
          + "'' and em_atecfe_documento_xml is not null;')";

      ResultSet resulSet = statem2.executeQuery(strSql);

      try {
        if (resulSet.next() && hstDatos.get("dstd").equals("AP")) {

          String strpInstaID = UUID.randomUUID().toString().replace("-", "").toUpperCase();

          strSql = "";

          strSql = "SELECT dblink_exec('dbname="
              + clsCli.getBdd()
              + " hostaddr="
              + clsCli.getIp()
              + " user="
              + clsCli.getUsr()
              + " password="
              + clsCli.getPass()
              + " port="
              + clsCli.getPuerto()
              + "','insert into ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
              + "ad_user_id, updated, result, errormsg, ad_client_id, "
              + "ad_org_id, createdby, updatedby, isactive)" + "values (''" + strpInstaID
              + "'', (select ad_process_id from ad_process "
              + "where upper(procedurename) = upper(''c_invoice_post0'') "
              + "and upper(name) = upper(''Process Invoice'')), ''" + strDocID
              + "'', ''N'', now()," + "(select createdby from c_invoice where c_invoice_id = ''"
              + strDocID + "''), now(),''0'','''', (select ad_client_id from c_invoice "
              + "where c_invoice_id = ''" + strDocID + "''), "
              + "(select ad_org_id from c_invoice " + "where c_invoice_id = ''" + strDocID
              + "''), ''0'', ''0'', ''Y'');')";

          resulSet = null;

          resulSet = statem2.executeQuery(strSql);

          strSql = "SELECT dblink_exec('dbname=" + clsCli.getBdd() + " hostaddr=" + clsCli.getIp()
              + " user=" + clsCli.getUsr() + " password=" + clsCli.getPass() + " port="
              + clsCli.getPuerto() + "','select c_invoice_post(''" + strpInstaID + "'',''"
              + strDocID + "'');')";

          resulSet = null;

          resulSet = statem2.executeQuery(strSql);
        }
      } catch (Exception e) {
        // Mensaje Error
      }

      strSql = "SELECT dblink_exec('dbname=" + clsCli.getBdd() + " hostaddr=" + clsCli.getIp()
          + " user=" + clsCli.getUsr() + " password=" + clsCli.getPass() + " port="
          + clsCli.getPuerto() + "',"
          + "'update co_retencion_compra set em_atecfe_documento_xml = decode(''"
          + hstDatos.get("doc") + "'',''base64''), " + "em_atecfe_menobserror_sri = ''" + mensaje2
          + "'', " + "em_atecfe_docaction = ''" + hstDatos.get("std") + "'', "
          + "em_atecfe_docstatus = ''" + hstDatos.get("dstd") + "'', no_autorizacion = ''"
          + hstDatos.get("numaut") + "'', em_atecfe_fecha_autori = ''" + hstDatos.get("fecaut")
          + "'' where " + "co_retencion_compra_id = ''" + strDocID
          + "'' and em_atecfe_documento_xml is not null;')";

      resulSet = null;
      resulSet = statem2.executeQuery(strSql);

      try {
        if (resulSet.next() && hstDatos.get("dstd").equals("AP")) {
          String strpInstaID = UUID.randomUUID().toString().replace("-", "").toUpperCase();

          strSql = "";

          strSql = "SELECT dblink_exec('dbname="
              + clsCli.getBdd()
              + " hostaddr="
              + clsCli.getIp()
              + " user="
              + clsCli.getUsr()
              + " password="
              + clsCli.getPass()
              + " port="
              + clsCli.getPuerto()
              + "','insert into ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
              + "ad_user_id, updated, result, errormsg, ad_client_id, "
              + "ad_org_id, createdby, updatedby, isactive)"
              + "values (''"
              + strpInstaID
              + "'', (select ad_process_id from ad_process where upper(procedurename) = upper(''co_ejecuta_retencion_compra'') "
              + "and upper(name) = upper(''Completar Retención Compra'')), ''" + strDocID
              + "'', ''N'', now(),"
              + "(select createdby from co_retencion_compra where co_retencion_compra_id = ''"
              + strDocID + "''), now(),''0'','''', (select ad_client_id from co_retencion_compra "
              + "where co_retencion_compra_id = ''" + strDocID + "''), "
              + "(select ad_org_id from co_retencion_compra where co_retencion_compra_id = ''"
              + strDocID + "''), ''0'', ''0'', ''Y'');')";

          resulSet = null;

          resulSet = statem2.executeQuery(strSql);

          try {
            strSql = "";

            strSql = "SELECT dblink_exec('dbname=" + clsCli.getBdd() + " hostaddr="
                + clsCli.getIp() + " user=" + clsCli.getUsr() + " password=" + clsCli.getPass()
                + " port=" + clsCli.getPuerto() + "','select ad_pinstance_para_insert(''"
                + strpInstaID
                + "'', ''10'', ''AccionRet'', ''CO'', null, ''0'', ''0'', null, null);')";

            resulSet = null;

            resulSet = statem2.executeQuery(strSql);
          } catch (Exception e) {
            // Mensaje Error
          }

          try {
            strSql = "";

            strSql = "SELECT dblink_exec('dbname=" + clsCli.getBdd() + " hostaddr="
                + clsCli.getIp() + " user=" + clsCli.getUsr() + " password=" + clsCli.getPass()
                + " port=" + clsCli.getPuerto() + "','select co_ejecuta_retencion_compra(''"
                + strpInstaID + "'');')";

            resulSet = null;

            resulSet = statem2.executeQuery(strSql);
          } catch (Exception e) {
            // Mensaje Error
          }

        }
      } catch (Exception e) {
        // Mensaje Error
      }

      if (resulSet != null) {

        statem2.close();
        connec2.close();
        resulSet.close();
        return true;
      } else {
        statem2.close();
        connec2.close();
        return false;
      }

    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
      return false;
    }
  }

  /*
   * Funci�n que guarda los datos en el cliente devueltos del WS
   */
  private boolean actualizarTransEstado(String StrTranId, String strEstado, String strComentario,
      DataSource dataSour, boolean blnRec) {
    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strComentario2 = strComentario.replaceAll("'", "''");

      String strSql = "";

      if (blnRec) {
        strSql = "Update transacciones set estado='" + strEstado + "' , comentario = '"
            + strComentario2 + "' , doc_rec = 'Y' where transaccion_id = '" + StrTranId + "'";
      } else {
        strSql = "Update transacciones set estado='" + strEstado + "' , comentario = '"
            + strComentario2 + "' where transaccion_id = '" + StrTranId + "'";
      }

      System.out.println(strSql);

      int resulSet = statem2.executeUpdate(strSql);

      if (resulSet == 1) {
        statem2.close();
        connec2.close();
        return true;
      } else {
        statem2.close();
        connec2.close();
        return false;
      }
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
      return false;
    }
  }

  public void actualizarTransTotal() {
    try {

      Context initConte;

      /*
       * Cargando los datos del context
       */
      initConte = new InitialContext();
      Context envConte = (Context) initConte.lookup("java:/comp/env");

      DataSource dataSour = null;

      dataSour = (DataSource) envConte.lookup("jdbc/postgres");

      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "Update transacciones set estado='RG', comentario = '' where estado = 'TR'";

      int resulSet = statem2.executeUpdate(strSql);

      if (resulSet == 1) {
        statem2.close();
        connec2.close();
      } else {
        statem2.close();
        connec2.close();
      }
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
  }

  /*
   * Funci�n que guarda los datos en el cliente devueltos del WS
   */
  private boolean insertarTransa(String strClient, String strDoc, String strInvoice,
      Hashtable<String, String> hstDatos, DataSource dataSour) {

    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "select transaccion_id, doc_rec from transacciones where " + "doc_id = '"
          + strInvoice + "'";

      ResultSet resulSet2 = statem2.executeQuery(strSql);

      if (resulSet2.next()) {
        hstDatos.put("clvtra", resulSet2.getString("transaccion_id"));
        hstDatos.put("rec", resulSet2.getString("doc_rec"));
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "PD");
        resulSet2.close();
        statem2.close();
        connec2.close();
        return true;
      } else {
        hstDatos.put("clvtra", UUID.randomUUID().toString().replace("-", "").toUpperCase());
        hstDatos.put("rec", "N");

        strSql = "Insert into transacciones(transaccion_id" + ", cliente_id" + ", tipo_doc"
            + ", doc_id" + ", estado, doc_rec) " + "values('" + hstDatos.get("clvtra") + "', '"
            + strClient + "', '" + strDoc + "', '" + strInvoice + "', 'PD','N')";

        int resulSet = statem2.executeUpdate(strSql);

        if (resulSet == 1) {
          statem2.close();
          connec2.close();
          hstDatos.remove("dstd");
          hstDatos.put("dstd", "PD");
          return true;
        } else {
          statem2.close();
          connec2.close();
          hstDatos.clear();
          hstDatos.put("std", "PR");
          hstDatos.put("dstd", "PD");
          hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
          return false;
        }
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      hstDatos.clear();
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      e.printStackTrace();
      return false;
    }
  }

  private boolean buscarTransa(String strClient, String strDoc, String strInvoice,
      Hashtable<String, String> hstDatos, DataSource dataSour) {

    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "select transaccion_id, doc_rec from transacciones where "
          + " cliente_id = '" + strClient + "' and" + " tipo_doc = '" + strDoc + "' and"
          + " doc_id = '" + strInvoice + "'";

      ResultSet resulSet = statem2.executeQuery(strSql);

      if (resulSet.next()) {
        hstDatos.put("clvtra", resulSet.getString("transaccion_id"));
        hstDatos.put("rec", resulSet.getString("doc_rec"));
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "PD");
        resulSet.close();
        statem2.close();
        connec2.close();
        return true;
      } else {
        resulSet.close();
        statem2.close();
        connec2.close();
        return (insertarTransa(strClient, strDoc, strInvoice, hstDatos, dataSour));
      }
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      hstDatos.clear();
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      e.printStackTrace();
      return false;
    }
  }

  /*
   * Funci�n que consulta los datos del cliente
   */
  private List<clientes> datosClis(DataSource dataSour) {
    List<clientes> lstClis = new ArrayList<clientes>();

    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "select c.cliente_id, c.nombre, c.string_bdd, c.usr, c.pass, c.activo, c.bdd, c.ip, c.puerto "
          + "from clientes c "
          + "inner join licencias l on (c.cliente_id = l.cliente_id) "
          + "where date(now()) <= date(l.fecha_vencimiento)" + "and c.activo = 'Y'";

      ResultSet resulSet = statem2.executeQuery(strSql);

      /*
       * Cargando los datos del cliente
       */
      if (resulSet != null) {
        lstClis = new ArrayList<clientes>();

        while (resulSet.next()) {
          clientes clsCli = new clientes();
          clsCli.setCliente_id(resulSet.getString("cliente_id"));
          clsCli.setNombre(resulSet.getString("nombre"));
          clsCli.setString_bdd(resulSet.getString("string_bdd"));
          clsCli.setUsr(resulSet.getString("usr"));
          clsCli.setPass(resulSet.getString("pass"));
          clsCli.setBdd(resulSet.getString("bdd"));
          clsCli.setIp(resulSet.getString("ip"));
          clsCli.setPuerto(resulSet.getString("puerto"));
          clsCli.setActivo(resulSet.getString("activo"));
          lstClis.add(clsCli);
        }
      }

      resulSet.close();
      statem2.close();
      connec2.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return lstClis;
  }

  private List<temEnt> buscarDocumentos(clientes clsCli, DataSource dataSour) {
    /*
     * Creando la lista de documentos
     */
    List<temEnt> lstDoc = new ArrayList<temEnt>();
    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "SELECT t.transaccion_id, a.em_atecfe_documento_xml, t.cliente_id, a.em_atecfe_codigo_acc, t.doc_id, t.tipo_doc, t.estado "
          + "FROM dblink('dbname="
          + clsCli.getBdd()
          + " hostaddr="
          + clsCli.getIp()
          + " user="
          + clsCli.getUsr()
          + " password="
          + clsCli.getPass()
          + " port="
          + clsCli.getPuerto()
          + "',"
          + "'select i.c_invoice_id, i.em_atecfe_codigo_acc, encode(i.em_atecfe_documento_xml,''base64'') as em_atecfe_documento_xml "
          + "from c_invoice i where i.em_atecfe_documento_xml is not null "
          + "union all select rc.co_retencion_compra_id as c_invoice_id, rc.em_atecfe_codigo_acc, encode(rc.em_atecfe_documento_xml,''base64'') "
          + "as em_atecfe_documento_xml from co_retencion_compra rc where rc.em_atecfe_documento_xml is not null') "
          + "as a(c_invoice_id varchar(32), em_atecfe_codigo_acc varchar(255), em_atecfe_documento_xml TEXT) "
          + "INNER JOIN transacciones t ON (a.c_invoice_id = t.doc_id) WHERE t.estado IN ('PD','RC','RG') "
          + "AND t.fecha_envio < NOW()-'10 minute'::interval AND a.em_atecfe_documento_xml IS NOT NULL";

      ResultSet resulSet = statem2.executeQuery(strSql);

      /*
       * Cargando los datos del c_invoice
       */

      if (resulSet != null) {
        lstDoc = new ArrayList<temEnt>();

        while (resulSet.next()) {
          temEnt tmp = new temEnt();
          tmp.setTransaccion(resulSet.getString("transaccion_id"));
          tmp.setDoc_xml(resulSet.getString("em_atecfe_documento_xml"));
          tmp.setCli_id(resulSet.getString("cliente_id"));
          tmp.setCla_acc(resulSet.getString("em_atecfe_codigo_acc"));
          tmp.setDoc_id(resulSet.getString("doc_id"));
          tmp.setTipo_doc(resulSet.getString("tipo_doc"));
          tmp.setEstado(resulSet.getString("estado"));
          if (actualizarTransEstado(tmp.getTransaccion(), "TR", "Tramitando Internamente",
              dataSour, false)) {
            lstDoc.add(tmp);
          }
        }
      }

      resulSet.close();
      statem2.close();
      if (!connec2.isClosed()) {
        connec2.close();
      }
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }

    return lstDoc;
  }

  public void imprimirMensaje(String strTitulo, String strMensaje, DataSource dataSour) {
    System.out.println(strTitulo + " " + calendario.get(Calendar.HOUR) + ":"
        + calendario.get(Calendar.MINUTE) + ":" + calendario.get(Calendar.SECOND) + ": "
        + strMensaje);
  }

  /*
   * Funci�n para verificar la licencia del cliente
   */

  private boolean verificarCliente(String strClient, Hashtable<String, String> hstDatos,
      DataSource dataSour) {
    /*
     * Creando la lista de documentos
     */

    try {
      Connection connec2 = dataSour.getConnection();
      Statement statem2 = connec2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
          ResultSet.CONCUR_READ_ONLY);

      String strSql = "select * from clientes c "
          + "Inner join licencias l on (c.cliente_id = l.cliente_id) "
          + "where date(l.fecha_vencimiento) >= date(now()) " + "and c.cliente_id = '" + strClient
          + "'";

      ResultSet resulSet = statem2.executeQuery(strSql);

      if (resulSet.next()) {
        hstDatos.clear();
        hstDatos.put("std", "PR");
        hstDatos.put("dstd", "PD");
        hstDatos.put("numaut", "");
        hstDatos.put("fecaut", "");
        resulSet.close();
        statem2.close();
        connec2.close();
        return true;
      } else {
        hstDatos.clear();
        hstDatos.put("mens", "Licencia Caducada");
        hstDatos.put("std", "PR");
        hstDatos.put("dstd", "RZ");
        hstDatos.put("numaut", "");
        hstDatos.put("fecaut", "");
        statem2.close();
        connec2.close();
        return false;
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      hstDatos.clear();
      hstDatos.put("mens", "Falla del servidor intentelo mas tarde");
      hstDatos.put("std", "PR");
      hstDatos.put("dstd", "PD");
      hstDatos.put("numaut", "");
      hstDatos.put("fecaut", "");
      return false;
    }
  }
}