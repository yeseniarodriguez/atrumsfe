package com.atrums.sri.ws;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Hashtable;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class recepcionComprobante {

  /*
   * Funci�n para realizar el consumo del Web service de recepci�n
   */
  public static boolean recepcionXML(String strwsdlRec, String strNameSpace, String strXml,
      Integer intNumInt, Integer intMaxNumInt, List<String> lsList) {

    try {
      /*
       * Realizando la conexi�n con el Web service
       */
      SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();

      /*
       * Realizando el thread para verificar la conexion
       */
      System.out.println("Mandado al Ambiente: " + strwsdlRec);
      
      URL endPoint = new URL(null, strwsdlRec, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          // TODO Auto-generated method stub
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          /*
           * Asignando tiempos de respuesta maximos
           */
          clone_urlconConnection.setConnectTimeout(5000);
          clone_urlconConnection.setReadTimeout(5000);
          return clone_urlconConnection;
        }
      });

      /*
       * Realizando la consulta al Web service de recepci�n
       */
      SOAPMessage soapMes = soapCon.call(createSoapReqRecXML(strNameSpace, strXml), endPoint);

      /*
       * Capturando que estado devolvio del Web service de la petici�n
       */
      String strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(0)
          .getFirstChild().getNodeValue();

      /*
       * Verificando que resultado dio el Web Service
       */
      if (strEstado.equals("DEVUELTA")) {
        lsList.clear();

        /*
         * Armando el mensaje de respuesta
         */
        lsList.add(soapMes.getSOAPBody().getElementsByTagName("mensaje").item(1).getFirstChild()
            .getNodeValue());
        lsList.add(soapMes.getSOAPBody().getElementsByTagName("informacionAdicional").item(0)
            .getFirstChild().getNodeValue());
        soapCon.close();
        return false;
      } else if (strEstado.equals("RECIBIDA")) {
        soapCon.close();
        return true;
      }
    } catch (Exception e) {
      // TODO: handle exception

      /*
       * Verificando que cuantos intentos tiene realizado la consulta
       */
      if (intNumInt <= intMaxNumInt) {
        if (recepcionXML(strwsdlRec, strNameSpace, strXml, intNumInt + 1, intMaxNumInt, lsList)) {
          return true;
        } else {
          return false;
        }
      } else {
        /*
         * Armando el mensaje de respuesta
         */
        lsList.clear();
        lsList.add("El Web Service no disponible del SRI para Recibir el documento");
        return false;
      }
    }
    return false;
  }

  /*
   * Funci�n para realizar el consumo del Web service de recepci�n
   */
  public static boolean recepcionDocXML(String strwsdlRec, String strNameSpace, String strXml,
      Integer intNumInt, Integer intMaxNumInt, Hashtable<String, String> hstDatos) {

    try {
      /*
       * Realizando la conexi�n con el Web service
       */
      SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();

      /*
       * Realizando el thread para verificar la conexion
       */
      
      System.out.println("Mandado al Ambiente: " + strwsdlRec);
      
      URL endPoint = new URL(null, strwsdlRec, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          // TODO Auto-generated method stub
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          /*
           * Asignando tiempos de respuesta maximos
           */
          clone_urlconConnection.setConnectTimeout(60000);
          clone_urlconConnection.setReadTimeout(60000);
          return clone_urlconConnection;
        }
      });

      /*
       * Realizando la consulta al Web service de recepci�n
       */
      SOAPMessage soapMes = soapCon.call(createSoapReqRecXML(strNameSpace, strXml), endPoint);

      /*
       * Capturando que estado devolvio del Web service de la petici�n
       */
      String strEstado = soapMes.getSOAPBody().getElementsByTagName("estado").item(0)
          .getFirstChild().getNodeValue();

      /*
       * Verificando que resultado dio el Web Service
       */
      if (strEstado.equals("DEVUELTA")) {

        String strMensajeAux = soapMes.getSOAPBody().getElementsByTagName("mensaje").item(1)
            .getFirstChild().getNodeValue();

        if (strMensajeAux.indexOf("CLAVE DE ACCESO EN PROCESAMIENTO") != -1) {
          /*
           * Armando el mensaje de respuesta
           */
          strMensajeAux = "La factura sera tramitada automaticamente " + strMensajeAux;

          if (strMensajeAux.length() > 254) {
            hstDatos.put("mens", strMensajeAux.substring(0, 254));
          } else {
            hstDatos.put("mens", strMensajeAux);
          }

          // hstDatos.put("mens", "La Factura fue recibida por el SRI");
          hstDatos.remove("dstd");
          hstDatos.put("dstd", "RC");
          soapCon.close();
          return true;
        }

        /*
         * Armando el mensaje de respuesta
         */
        hstDatos.put("doc", strXml);
        hstDatos.put("mens", soapMes.getSOAPBody().getElementsByTagName("mensaje").item(1)
            .getFirstChild().getNodeValue()
            + " - "
            + soapMes.getSOAPBody().getElementsByTagName("informacionAdicional").item(0)
                .getFirstChild().getNodeValue());
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "RZ");
        soapCon.close();
        return false;
      } else if (strEstado.equals("RECIBIDA")) {

        /*
         * Armando el mensaje de respuesta
         */
        hstDatos.put("mens", "La Factura fue recibida por el SRI");
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "RC");
        soapCon.close();
        return true;
      } else {
        /*
         * Armando el mensaje de respuesta
         */
        hstDatos.put("doc", strXml);
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "RG");
        hstDatos.put("mens", "Ocurrio un error en el servicio del SRI");
        return false;
      }
    } catch (Exception e) {
      // TODO: handle exception

      /*
       * Verificando que cuantos intentos tiene realizado la consulta
       */
      if (intNumInt <= intMaxNumInt) {
        if (recepcionDocXML(strwsdlRec, strNameSpace, strXml, intNumInt + 1, intMaxNumInt, hstDatos)) {
          return true;
        } else {
          return false;
        }
      } else {
        /*
         * Armando el mensaje de respuesta
         */
        hstDatos.put("doc", strXml);
        hstDatos.remove("dstd");
        hstDatos.put("dstd", "RG");
        hstDatos
            .put("mens",
                "El Servicio del SRI no esta disponible, su documento sera procesado mas tarde automaticamente");
        return false;
      }
    }
  }

  /*
   * Funci�n para la creaci�n de la consulta soap para el Web Service de recepci�n del documento por
   * el SRI
   */
  private static SOAPMessage createSoapReqRecXML(String strNameSpace, String strXml)
      throws SOAPException, IOException {
    MessageFactory messageFac = MessageFactory.newInstance();
    SOAPMessage soapMes = messageFac.createMessage();
    SOAPPart soapPart = soapMes.getSOAPPart();

    /*
     * Configuraci�n el Namespace del Web Service
     */
    SOAPEnvelope soapEnv = soapPart.getEnvelope();
    soapEnv.addNamespaceDeclaration("ec", strNameSpace);

    SOAPBody soapBod = soapEnv.getBody();

    /*
     * Configuraci�n de los parametros pasados al Web Service
     */
    SOAPElement soapEle = soapBod.addChildElement("validarComprobante", "ec");
    SOAPElement soapEle1 = soapEle.addChildElement("xml");
    soapEle1.addTextNode(strXml);

    /*
     * Guardando los cambios de la configuraci�n
     */
    soapMes.saveChanges();

    return soapMes;
  }

}
