package com.atrums.entidades;

public class temEnt {

  private String transaccion;
  private String doc_xml;
  private String cli_id;
  private String cla_acc;
  private String doc_id;
  private String tipo_doc;
  private String estado;

  public temEnt() {
    super();
  }

  public String getTransaccion() {
    return transaccion;
  }

  public void setTransaccion(String transaccion) {
    this.transaccion = transaccion;
  }

  public String getDoc_xml() {
    return doc_xml;
  }

  public void setDoc_xml(String doc_xml) {
    this.doc_xml = doc_xml;
  }

  public String getCli_id() {
    return cli_id;
  }

  public void setCli_id(String cli_id) {
    this.cli_id = cli_id;
  }

  public String getCla_acc() {
    return cla_acc;
  }

  public void setCla_acc(String cla_acc) {
    this.cla_acc = cla_acc;
  }

  public String getDoc_id() {
    return doc_id;
  }

  public void setDoc_id(String doc_id) {
    this.doc_id = doc_id;
  }

  public String getTipo_doc() {
    return tipo_doc;
  }

  public void setTipo_doc(String tipo_doc) {
    this.tipo_doc = tipo_doc;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }
}
