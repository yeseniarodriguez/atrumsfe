package com.atrums.entidades;

public class invoice {

  /*
   * clase de la tabla c_invoice de la base de datos solo con los campos de c_invoice_id, documento
   * XML y clave de accesos
   */
  private String c_invoice_id;
  private String docXML;
  private String claveAcc;

  public invoice() {
    super();
  }

  public String getC_invoice_id() {
    return c_invoice_id;
  }

  public void setC_invoice_id(String c_invoice_id) {
    this.c_invoice_id = c_invoice_id;
  }

  public String getDocXML() {
    return docXML;
  }

  public void setDocXML(String docXML) {
    this.docXML = docXML;
  }

  public String getClaveAcc() {
    return claveAcc;
  }

  public void setClaveAcc(String claveAcc) {
    this.claveAcc = claveAcc;
  }
}
