package com.atrums.entidades;

public class clientes {

  /*
   * clase de la tabla clientes de la base de datos
   */

  private String cliente_id;
  private String nombre;
  private String string_bdd;
  private String usr;
  private String pass;
  private String bdd;
  private String ip;
  private String puerto;
  private String activo;

  public clientes() {
    super();
  }

  public String getCliente_id() {
    return cliente_id;
  }

  public void setCliente_id(String cliente_id) {
    this.cliente_id = cliente_id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getString_bdd() {
    return string_bdd;
  }

  public void setString_bdd(String string_bdd) {
    this.string_bdd = string_bdd;
  }

  public String getUsr() {
    return usr;
  }

  public void setUsr(String usr) {
    this.usr = usr;
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  public String getBdd() {
    return bdd;
  }

  public void setBdd(String bdd) {
    this.bdd = bdd;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getPuerto() {
    return puerto;
  }

  public void setPuerto(String puerto) {
    this.puerto = puerto;
  }

  public String getActivo() {
    return activo;
  }

  public void setActivo(String activo) {
    this.activo = activo;
  }

}
